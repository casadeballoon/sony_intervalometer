// Arduino Pro 3.3V 8MHz Atmega328


#define Red_LED  13
#define NEX_RED  2
#define NEX_WHITE  3


void setup() 
{
  pinMode(Red_LED, OUTPUT);
  pinMode(NEX_RED, OUTPUT);
  pinMode(NEX_WHITE, OUTPUT);
  digitalWrite(Red_LED, 0);
  digitalWrite(NEX_RED, 1);
  digitalWrite(NEX_WHITE, 1);
  delay(10000);  
}



uint32_t lastCam = micros();

void loop()
{
  uint32_t time = micros();
  digitalWrite(Red_LED, 1);
  digitalWrite(NEX_RED, 0);
  delay(2000);
  digitalWrite(NEX_WHITE, 0);
  delay(2000);
  digitalWrite(Red_LED, 0);
  delay(500);
  digitalWrite(NEX_RED, 1);
  digitalWrite(NEX_WHITE, 1);
  delay(15500);
}


